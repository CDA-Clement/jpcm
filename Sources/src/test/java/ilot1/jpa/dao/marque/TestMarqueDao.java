package ilot1.jpa.dao.marque;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import ilot1.jpa.entity.Marque;
import ilot1.jpa.entity.Modele;

@TestMethodOrder(value = OrderAnnotation.class)
class TestMarqueDao {
	static MarqueDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new MarqueDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Marque marque;


	@Order(1)
	@Test
	void testAdd() {
		Marque m = new Marque();
		m.setLabel("Peugeot");
		marque = dao.add(m);
		assertNotNull(marque);
		assertNotEquals(0, marque.getCode());
	}

	@Order(2)
	@Test
	void testFind() {
		Marque findmarque= dao.find(marque.getCode());
		System.err.println(findmarque);
		assertNotNull(findmarque);
		assertEquals(marque.getCode(), findmarque.getCode());
	}

	@Order(3)
	@Test
	void testUpdate() {
		System.err.println(marque.getLabel());
		marque.setLabel("Renault");
		dao.update(marque);
		Marque m =marque;
		System.err.println(m.getLabel());
		assertFalse(m.getLabel().equalsIgnoreCase("Peugeot"));
	}
	
	@Order(4)
	@Test
	void testRemove() {
		dao.find(marque.getCode());
		Collection<Marque> marques = dao.findAll();
		int avant =marques.size();
		for (Marque marque : marques) {
			System.err.println(marque);	
		}
		dao.remove(marque.getCode());
		marques = dao.findAll();
		int apres=marques.size();
		assertTrue(apres<avant);
	}

}
