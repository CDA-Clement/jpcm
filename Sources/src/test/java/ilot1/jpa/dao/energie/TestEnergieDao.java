package ilot1.jpa.dao.energie;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import ilot1.jpa.entity.Energie;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;


@TestMethodOrder(value = OrderAnnotation.class)
class TestEnergieDao {
	static EnergieDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new EnergieDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Energie energie;


	@Order(1)
	@Test
	void testAdd() {
		Energie m = new Energie();
		m.setLabel("diesel");
		
		

		energie = dao.add(m);
		assertNotNull(energie);
		assertNotEquals(0, energie.getCode());
	}

	@Order(2)
	@Test
	void testFind() {
		Energie findmodele= dao.find(energie.getCode());
		System.err.println(findmodele);
		assertNotNull(findmodele);
		assertEquals(energie.getCode(), findmodele.getCode());
	}


	@Test
	void testUpdate() {
		energie.setLabel("essence");
		dao.update(energie);
		Energie m =energie;
		assertFalse(m.getLabel().equalsIgnoreCase("diesel"));
	}

	@Test
	void testRemove() {
		dao.find(energie.getCode());
		Collection<Energie> energies = dao.findAll();
		int avant =energies.size();
		for (Energie energie : energies) {
			System.err.println(energie);	
		}
		dao.remove(energie.getCode());
		energies = dao.findAll();
		int apres=energies.size();
		assertTrue(apres<avant);
	}


}
