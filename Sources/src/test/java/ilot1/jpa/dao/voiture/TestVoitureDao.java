package ilot1.jpa.dao.voiture;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;

import ilot1.jpa.dao.couleur.CouleurDao;
import ilot1.jpa.dao.voiture.VoitureDao;
import ilot1.jpa.entity.Couleur;
import ilot1.jpa.entity.Voiture;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import org.junit.jupiter.api.Order;




@TestMethodOrder(value = OrderAnnotation.class)
class TestVoitureDao {

	static VoitureDao dao;
	static CouleurDao daoc;

	@BeforeAll
	public static void beforeAll() {
		dao = new VoitureDao();
		 daoc=new CouleurDao();
		couleur = new Couleur();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Voiture voiture;
	private static Couleur couleur;

	@Order(1)
	@Test
	void testAdd() {
		couleur=daoc.find(142);
		
		Voiture v = new Voiture();
		v.setCouleur(couleur);
		//	v.setModele("model 1");
		voiture = dao.add(v);
		assertNotNull(voiture);
		assertNotEquals(0, voiture.getMatricule());

	}


	@Order(2)
	@Test
	void testFind() {
		System.err.println("+++++++++++++++++++++++++ " +voiture.getMatricule());
		Voiture findvoiture= dao.findWithVoitures(158);
		//System.err.println(findvoiture);
		assertNotNull(findvoiture);
		assertEquals(voiture.getModele(), findvoiture.getModele());

	}


	@Order(3)
	@Test
	void testUpdate() {
		
		voiture=dao.find(148);		
		voiture.setCouleur(daoc.find(147));
		dao.update(voiture);
		Voiture v =voiture;
		assertFalse(v.getCouleur().getLabel().equalsIgnoreCase("Bleu"));
	}
	
	@Disabled
	@Order(4)
	@Test
	void testRemove() {
		dao.find(voiture.getMatricule());
		Collection<Voiture> voitures = dao.findAll();
		int avant =voitures.size();
		dao.remove(voiture.getMatricule());
		voitures = dao.findAll();
		int apres=voitures.size();
		assertTrue(apres<avant);

	}
	
	
	@Order(5)
	@Test
	void testFindByColor() {
		Collection<Voiture> voitures= dao.findByColor("rose");
		assertTrue(voitures.size()==0);
		voitures= dao.findByColor("rouge");
		for (Voiture voiture : voitures) {
			System.err.println(voiture);
			
		}
		assertTrue(voitures.size()>1);
	}
	
	@Order(5)
	@Test
	void testFindByModele() {
		Collection<Voiture> voitures= dao.findByModele("model 0");
		assertTrue(voitures.size()==0);
		voitures= dao.findByModele("model 1");
		for (Voiture voiture : voitures) {
			System.err.println("*********************"+voiture);
			
		}
		assertTrue(voitures.size()>1);
	}
	

	@Test
	void testFindMaxPowerByBrand() {
		
	}



}
