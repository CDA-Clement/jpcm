package ilot1.jpa.dao.couleur;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import ilot1.jpa.dao.couleur.CouleurDao;
import ilot1.jpa.entity.Couleur;
import ilot1.jpa.entity.Voiture;

@TestMethodOrder(value = OrderAnnotation.class)
class TestCouleurDao {
	static CouleurDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new CouleurDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Couleur couleur;
	@Order(1)
	@Test
	void testAdd() {
		Couleur c = new Couleur();
		c.setLabel("Marron");

		couleur = dao.add(c);
		assertNotNull(couleur);
		assertNotEquals(0, couleur.getCode());



	}
	@Order(2)
	@Test
	void testFind() {
		Couleur findcouleur= dao.findWithVoitures(couleur.getCode());
		System.err.println(findcouleur.getLabel());
		System.err.println("+++++++++++++++++++++++++++++++++++4"+findcouleur);
		assertNotNull(findcouleur);
		assertEquals(couleur.getCode(), findcouleur.getCode());

	}
	@Order(3)
	@Test
	void testUpdate() {
		couleur.setLabel("Gris");
		dao.update(couleur);
		Couleur v =couleur;
		assertFalse(v.getLabel().equalsIgnoreCase("Marron"));
	}
	@Order(4)
	@Test
	void testRemove() {
		dao.find(couleur.getCode());
		Collection<Couleur> couleurs = dao.findAll("findAllc");
		int avant =couleurs.size();
		for (Couleur couleur : couleurs) {
			System.err.println("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"+couleur);	
		}
		dao.remove(couleur.getCode());
		couleurs = dao.findAll("findAllc");
		int apres=couleurs.size();
		assertTrue(apres<avant);
	}

	



}
