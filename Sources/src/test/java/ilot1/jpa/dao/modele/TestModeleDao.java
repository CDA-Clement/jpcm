package ilot1.jpa.dao.modele;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Collection;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import ilot1.jpa.dao.modele.ModeleDao;

import ilot1.jpa.entity.Modele;


@TestMethodOrder(value = OrderAnnotation.class)
class TestModeleDao {
	static ModeleDao dao;

	@BeforeAll
	public static void beforeAll() {
		dao = new ModeleDao();
	}

	@AfterAll
	public static void afterAll() {
		dao.close();
	}

	private static Modele modele;


	@Order(1)
	@Test
	void testAdd() {
		Modele m = new Modele();
		m.setLabel("C3d");
		m.setEnergie("diesel");
		m.setLabel("citadine");
		m.setMarque("Citroen");
		m.setPuissance(90);

		modele = dao.add(m);
		assertNotNull(modele);
		assertNotEquals(0, modele.getCode());
	}

	@Order(2)
	@Test
	void testFind() {
		Modele findmodele= dao.findWithVoitures(modele.getCode());
		System.err.println(findmodele);
		assertNotNull(findmodele);
		assertEquals(modele.getCode(), findmodele.getCode());
	}


	@Test
	void testUpdate() {
		modele.setLabel("C4d");
		dao.update(modele);
		Modele m =modele;
		assertFalse(m.getLabel().equalsIgnoreCase("C3d"));
	}

	@Test
	void testRemove() {
		dao.find(modele.getCode());
		Collection<Modele> modeles = dao.findAll("findAllm");
		int avant =modeles.size();
		for (Modele modele : modeles) {
			System.err.println(modele);	
		}
		dao.remove(modele.getCode());
		modeles = dao.findAll("findAllm");
		int apres=modeles.size();
		assertTrue(apres<avant);
	}


}
