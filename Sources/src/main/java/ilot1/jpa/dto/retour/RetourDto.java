package ilot1.jpa.dto.retour;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RetourDto {
	CodeRetour code;
	String meg;
}
