package ilot1.jpa.dto;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(of = { "code", "label", "puissance", "marque", "energie" })
public class ModeleDto {
	private Integer code;
	private String label;
	private Integer puissance;
	private String marque;
	private String energie;

	ModeleDto() {

	}
}