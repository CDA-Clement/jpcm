package ilot1.jpa.dto;

import java.util.Set;

import ilot1.jpa.entity.Modele;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(of = { "code", "label", "modeles" })
public class EnergieDto {
	private Integer code;
	private String label;
	private Set<Modele> modeles;

	EnergieDto() {

	}
}