package ilot1.jpa.dto;

import java.util.Set;

import ilot1.jpa.entity.Voiture;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(of = { "code", "label", "voitures" })
public class CouleurDto {
	private String code;
	private String label;
	private Set<Voiture> voitures;

	CouleurDto() {

	}
}