package ilot1.jpa.dto;

import ilot1.jpa.entity.Couleur;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VoitureCreationDto {
	private String modele;
	private Couleur couleur;
}
