package ilot1.jpa.ihm;

import java.util.Scanner;

import ilot1.jpa.dto.VoitureCreationDto;
import ilot1.jpa.dto.retour.CodeRetour;
import ilot1.jpa.dto.retour.RetourDto;
import ilot1.jpa.service.Clavier;
import ilot1.jpa.service.couleur.AjoutCouleur;
import ilot1.jpa.service.couleur.ListerCouleur;
import ilot1.jpa.service.energie.AjoutEnergie;
import ilot1.jpa.service.energie.ListerEnergie;
import ilot1.jpa.service.exception.AjoutInvalideException;
import ilot1.jpa.service.marque.AjoutMarque;
import ilot1.jpa.service.marque.ListerMarque;
import ilot1.jpa.service.modele.AjoutModele;
import ilot1.jpa.service.modele.ListerModele;
import ilot1.jpa.service.voiture.AjoutVoiture;
import ilot1.jpa.service.voiture.ListerVoiture;
import ilot1.jpa.service.voiture.SupprimerVoiture;

public class Menu {
	public static void main(String[] args) throws AjoutInvalideException {
		Scanner sc = new Scanner(System.in);
		boolean continueProg = true;
		int choix;
		String strParam1;
		String strParam2;
		String strParam3;
		int intParam1;
		
		do {
			System.out.println("################################################################");
			System.out.println("######################### Menu-Garage ##########################");
			System.out.println("################################################################");
			System.out.println("# 1 - Créer                                                    #");
			System.out.println("# 2 - Lister                                                   #");
			System.out.println("# 3 - Mise à jour                                              #");
			System.out.println("# 4 - Supprimer                                                #");
			System.out.println("# 5 - Quitter                                                  #");
			choix = Clavier.lireEntier(sc);
			switch (choix) {
			case 1:
				System.out.println("################################################################");
				System.out.println("## 1 - Ajouter une voiture                                     #");
				System.out.println("## 2 - Ajouter une marque                                      #");
				System.out.println("## 3 - Ajouter un modèle                                       #");
				System.out.println("## 4 - Ajouter une énergie                                     #");
				System.out.println("## 5 - Ajouter une couleur                                     #");
				System.out.println("## 6 - Retour                                                  #");
				choix = Clavier.lireEntier(sc);
				switch (choix) {
				case 1:
					System.out.println("Ajouter une voiture :");
					System.out.println(" - Saisir le modèle :");
					strParam1 = Clavier.lireTxt(sc);
					System.out.println(" - Saisir la couleur :");
					strParam2 = Clavier.lireTxt(sc);
					RetourDto ajouterUneVoiture = AjoutVoiture.ajouterUneVoiture(VoitureCreationDto.builder().modele(strParam1).build());
					if(ajouterUneVoiture.getCode() == CodeRetour.OK) {
						System.out.println(ajouterUneVoiture.getMeg());
					} else {
						System.err.println(ajouterUneVoiture.getMeg());
					}
					break;
				case 2:
					System.out.println("Ajouter une marque :");
					System.out.println(" - Saisir le label :");
					strParam1 = Clavier.lireTxt(sc);
					System.out.println(AjoutMarque.ajouterUneMarque(strParam1));
					break;
				case 3:
					System.out.println("Ajouter un modèle :");
					System.out.println(" - Saisir la marque :");
					strParam1 = Clavier.lireTxt(sc);
					System.out.println(" - Saisir le label :");
					strParam2 = Clavier.lireTxt(sc);
					System.out.println(" - Saisir la puissance :");
					intParam1 = Clavier.lireEntier(sc);
					System.out.println(" - Saisir l'énergie :");
					strParam3 = Clavier.lireTxt(sc);
					System.out.println(AjoutModele.ajouterUnModele(strParam1, strParam2, intParam1, strParam3));
					break;
				case 4:
					System.out.println("Ajouter une énergie :");
					System.out.println(" - Saisir le label :");
					strParam1 = Clavier.lireTxt(sc);
					System.out.println(AjoutEnergie.ajouterUneEnergie(strParam1));
					break;
				case 5:
					System.out.println("Ajouter une couleur :");
					System.out.println(" - Saisir le label :");
					strParam1 = Clavier.lireTxt(sc);
					System.out.println(AjoutCouleur.ajouterUneCouleur(strParam1));
					break;
				case 6:
					System.out.println("Retour");
					break;
				default:
					System.out.println("Saisie invalide");
					break;
				}
				break;
			case 2:
				System.out.println("################################################################");
				System.out.println("## 1 - Lister les voitures                                     #");
				System.out.println("## 2 - Lister les marques                                      #");
				System.out.println("## 3 - Lister les modèles                                      #");
				System.out.println("## 4 - Lister les énergies                                     #");
				System.out.println("## 5 - Lister les couleurs                                     #");
				System.out.println("## 6 - Lister les voitures possèdent la couleur saisi          #");
				System.out.println("## 7 - Lister les voitures les plus puissante de chaque marque #");
				System.out.println("## 8 - Retour                                                  #");
				choix = Clavier.lireEntier(sc);
				switch (choix) {
				case 1:
					System.out.println("Lister les voitures :");
					ListerVoiture.listerVoiture().stream().forEach(System.out::println);
					break;
				case 2:
					System.out.println("Lister les marques :");
					ListerMarque.listerMarque().stream().forEach(System.out::println);
					break;
				case 3:
					System.out.println("Lister les modèles :");
					ListerModele.listerModele().stream().forEach(System.out::println);
					break;
				case 4:
					System.out.println("Lister les énergies :");
					ListerEnergie.listerEnergie().stream().forEach(System.out::println);
					break;
				case 5:
					System.out.println("Lister les couleurs :");
					ListerCouleur.listerCouleur().stream().forEach(System.out::println);
					break;
				case 6:
					System.out.println("Lister les voitures possèdent la couleur saisi :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 7:
					System.out.println("Lister les voitures les plus puissante de chaque marque :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 8:
					System.out.println("Retour");
					break;
				default:
					System.out.println("Saisie invalide");
					break;
				}
				break;
			case 3:
				System.out.println("################################################################");
				System.out.println("## 1 - Mise à jour d'une voiture                               #");
				System.out.println("## 2 - Mise à jour d'une marque                                #");
				System.out.println("## 3 - Mise à jour d'un modèle                                 #");
				System.out.println("## 4 - Mise à jour d'une énergie                               #");
				System.out.println("## 5 - Mise à jour d'une couleur                               #");
				System.out.println("## 6 - Retour                                                  #");
				choix = Clavier.lireEntier(sc);
				switch (choix) {
				case 1:
					System.out.println("Mise à jour d'une voiture :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 2:
					System.out.println("Mise à jour d'une marque :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 3:
					System.out.println("Mise à jour d'un modèle :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 4:
					System.out.println("Mise à jour d'une énergie :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 5:
					System.out.println("Mise à jour d'une couleur :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 6:
					System.out.println("Retour");
					break;
				default:
					System.out.println("Saisie invalide");
					break;
				}
				break;
			case 4:
				System.out.println("################################################################");
				System.out.println("## 1 - Suppression d'une voiture                               #");
				System.out.println("## 2 - Suppression d'une marque                                #");
				System.out.println("## 3 - Suppression d'un modèle                                 #");
				System.out.println("## 4 - Suppression d'une énergie                               #");
				System.out.println("## 5 - Suppression d'une couleur                               #");
				System.out.println("## 6 - Retour                                                  #");
				choix = Clavier.lireEntier(sc);
				switch (choix) {
				case 1:
					System.out.println("Suppression d'une voiture :");
					System.out.println(" - Saisir le matricule :");
					intParam1 = Clavier.lireEntier(sc);
					SupprimerVoiture.supprimerVoiture(intParam1);
					break;
				case 2:
					System.out.println("Suppression d'une marque :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 3:
					System.out.println("Suppression d'un modèle :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 4:
					System.out.println("Suppression d'une énergie :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 5:
					System.out.println("Suppression d'une couleur :");
					System.out.println("JE" + "\nSUIS" + "\nA" + "\nFAIRE" + "\n!!!");
					break;
				case 6:
					System.out.println("Retour");
					break;
				default:
					System.out.println("Saisie invalide");
					break;
				}
				break;
			case 5:
				continueProg = false;
				System.out.println("################################################################");
				System.out.println("########################## Au revoir ###########################");
				System.out.println("################################################################");
				break;
			default:
				System.out.println("Saisie Invalide");
			}
		} while (continueProg);
		sc.close();
	}
}
