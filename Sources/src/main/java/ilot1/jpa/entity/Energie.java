package ilot1.jpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@NamedQueries({ 
	@NamedQuery(name = "findAllnrj", query = "SELECT p FROM Energie p"),
//	@NamedQuery(name = "findByBrand", query = "SELECT p FROM Voiture p where p.= :fBrandParam"),
	@NamedQuery(name = "findByLabelnrj", query = "SELECT p FROM Energie p where p.label= :fLabelParam")
	})
@Table(name = "t_energie")
public class Energie {

		@Id()
		@GeneratedValue(strategy = GenerationType.AUTO)
		private Integer code;

		@Column(length = 150, nullable = false)
		private String label;
		
		// relation One(Energie) vers Many(Modele)
//		@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "monEnergie")
//		private Set<Modele> modeles;
		
}
