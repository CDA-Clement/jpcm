package ilot1.jpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

//import ilot1.jpa.entity.Modele.ModeleBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@Table(name = "t_couleur", uniqueConstraints = {@UniqueConstraint(columnNames = {"label"})})
@NamedQueries({ 
	@NamedQuery(name = "findAllc", query = "SELECT p FROM Couleur p"),
//	@NamedQuery(name = "findByBrand", query = "SELECT p FROM Voiture p where p.= :fBrandParam"),
	@NamedQuery(name = "findByLabel", query = "SELECT p FROM Couleur p where p.label= :fLabelParam")
	})
public class Couleur  {
	

	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer code;

	@Column(length = 150, nullable = false )
	private String label;

	// relation One(Couleur) vers Many(Voiture)
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "couleur")
	private Set<Voiture> voitures;

}