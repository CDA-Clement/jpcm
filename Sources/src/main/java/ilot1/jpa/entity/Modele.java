package ilot1.jpa.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
@NamedQueries({ 
	@NamedQuery(name = "findAllm", query = "SELECT p FROM Modele p"),
//	@NamedQuery(name = "findByBrand", query = "SELECT p FROM Voiture p where p.= :fBrandParam"),
	@NamedQuery(name = "findByCode", query = "SELECT p FROM Modele p where p.code= :fCodeParam"),
	@NamedQuery(name = "findByPower", query = "SELECT p FROM Modele p where p.label= :fPowerParam")
	})
@Table(name = "t_modele")
public class Modele {
	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer code;

	@Column(length = 150, nullable = false)
	private String label;

	@Column(length = 150, nullable = false)
	private Integer puissance;

	@Column(length = 150, nullable = false)
	private String marque;

	@Column(length = 150, nullable = false)
	private String energie;

	// relation Many(Modele) vers One(Marque)

//	@ManyToOne()
//	@JoinColumn(name = "modele")
//	private Marque modele;
//
//	// relation One(Modele) vers Many(Voiture)
//
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "modele")
	private Set<Voiture> listVoitures;
//
//	// relation Many(Modele) vers One(Energie)
//
//	@ManyToOne()
//	@JoinColumn(name = "energie")
//	private Energie energies;

}