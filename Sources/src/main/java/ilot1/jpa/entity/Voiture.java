package ilot1.jpa.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@Table(name = "t_voiture")
@NamedQueries({ 
	@NamedQuery(name = "findAll", query = "SELECT p FROM Voiture p"),
//	@NamedQuery(name = "findByBrand", query = "SELECT p FROM Voiture p where p.= :fBrandParam"),
	@NamedQuery(name = "findByColor", query = "SELECT p FROM Voiture p where p.couleur= :fCouleurParam"),
	@NamedQuery(name = "findBylModel", query = "SELECT p FROM Voiture p WHERE p.modele= :fModeleParam")
	})
public class Voiture {

	@Id()
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer matricule;


	// relation Many(Voiture) vers One(Modele)
	@ManyToOne()
	@JoinColumn(name = "modele")
	private Modele modele;

//	 relation Many(Voiture) vers One(couleur)
	@ManyToOne()
	@JoinColumn(name = "couleur")
	private Couleur couleur;

}