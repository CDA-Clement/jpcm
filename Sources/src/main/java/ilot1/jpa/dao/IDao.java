package ilot1.jpa.dao;

import java.util.Collection;

public interface IDao<T> {
	public void remove(Object pk);
	public T update(T entity);
	public T add(T entity);
	public Collection<T> findAllByNamedQuery(String query);
	public T find(Object id);
}


