package ilot1.jpa.dao.modele;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ilot1.jpa.dao.AbstractDao;
import ilot1.jpa.entity.Couleur;
import ilot1.jpa.entity.Modele;



public class ModeleDao extends AbstractDao<Modele>{

	private Class<Modele> clazz;
	private EntityManagerFactory factory = null;

	@SuppressWarnings("unchecked")
	public void AbstractDao() {
		this.clazz = (Class<Modele>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("myBase");
	}
	
	
	public Modele findWithVoitures(Object id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			Modele c = em.find(Modele.class, id);
			c.getListVoitures().size();
			return c;
		} finally {
			closeEntityManager(em);
		}
	}


	public Collection<Modele> findAll(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();

			TypedQuery<Modele> q = em.createNamedQuery(query, this.clazz);
			for (Modele modele : q.getResultList()) {
				modele.getListVoitures().size();
			}
			return q.getResultList();

		}finally {
			closeEntityManager(em);
		}
	}
}
