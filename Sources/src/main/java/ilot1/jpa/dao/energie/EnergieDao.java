package ilot1.jpa.dao.energie;

import java.util.Collection;

import ilot1.jpa.dao.AbstractDao;
import ilot1.jpa.entity.Energie;
import ilot1.jpa.entity.Marque;

public class EnergieDao extends AbstractDao<Energie> {
	
	public Collection<Energie> findAll() {
		return this.findAllByNamedQuery("findAllnrj");	
	}

}
