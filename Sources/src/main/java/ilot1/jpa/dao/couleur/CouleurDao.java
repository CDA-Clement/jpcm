package ilot1.jpa.dao.couleur;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ilot1.jpa.dao.AbstractDao;

import ilot1.jpa.entity.Couleur;

public class CouleurDao extends AbstractDao<Couleur> {
	private Class<Couleur> clazz;
	private EntityManagerFactory factory = null;

	@SuppressWarnings("unchecked")
	public void AbstractDao() {
		this.clazz = (Class<Couleur>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("myBase");
	}

// chargement de la liste de voitures qui sont li�es a une couleur
//lien avec la base de donnees.
	public Couleur findWithVoitures(Object id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			Couleur c = em.find(Couleur.class, id);
			c.getVoitures().size();
			return c;
		} finally {
			closeEntityManager(em);
		}
	}

	public Collection<Couleur> findAll(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();

			TypedQuery<Couleur> q = em.createNamedQuery(query, this.clazz);
			for (Couleur couleur : q.getResultList()) {
				couleur.getVoitures().size();
			}
			return q.getResultList();

		} finally {
			closeEntityManager(em);
		}
	}
}
