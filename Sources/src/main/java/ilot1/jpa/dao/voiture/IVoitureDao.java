package ilot1.jpa.dao.voiture;

import java.util.Collection;

import ilot1.jpa.dao.IDao;
import ilot1.jpa.entity.Voiture;

public interface IVoitureDao extends IDao<Voiture> {

	public Collection<Voiture> findByColor(String n);

	public Collection<Voiture> findMaxPowerByBrand();
}
