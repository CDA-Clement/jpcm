package ilot1.jpa.dao.voiture;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import ilot1.jpa.dao.AbstractDao;
import ilot1.jpa.entity.Couleur;
import ilot1.jpa.entity.Voiture;

public class VoitureDao extends AbstractDao<Voiture> implements IVoitureDao{
	
	private Class<Voiture> clazz;
	private EntityManagerFactory factory = null;

	@SuppressWarnings("unchecked")
	public void AbstractDao() {
		this.clazz = (Class<Voiture>) ((ParameterizedType) this.getClass().getGenericSuperclass())
				.getActualTypeArguments()[0];
		this.factory = Persistence.createEntityManagerFactory("myBase");
	}
	
	public Collection<Voiture> findByColor(String name) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Voiture> res = em.createNamedQuery("findByColor", Voiture.class);
			res.setParameter("fCouleurParam", name);
			return res.getResultList();
		} finally {
			closeEntityManager(em);
		}
	}

	public Collection<Voiture> findAll() {
		return this.findAllByNamedQuery("findAll");	
	}
	
	
	public Collection<Voiture> findMaxPowerByBrand() {
		return this.findAllByNamedQuery("findByBrand");
		
	}
	
	public Collection<Voiture> findByModele(String name) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			TypedQuery<Voiture> res = em.createNamedQuery("findBylModel", Voiture.class);
			res.setParameter("fModeleParam", name);
			return res.getResultList();
		} finally {
			closeEntityManager(em);
		}
		
	}
	
	public Voiture findWithVoitures(Object id) {
		EntityManager em = null;
		try {
			em = newEntityManager();
			Voiture v = em.find(Voiture.class, id);
			
			return v;
		} finally {
			closeEntityManager(em);
		}
	}
	
	public Collection<Voiture> findAll(String query) {
		EntityManager em = null;
		try {
			em = newEntityManager();

			TypedQuery<Voiture> q = em.createNamedQuery(query, this.clazz);
			for (Voiture voiture : q.getResultList()) {
				voiture.getCouleur();
				voiture.getModele();
			}
			return q.getResultList();

		} finally {
			closeEntityManager(em);
		}
	}
	
	
	
	
	
	
	
	
	

}
