package ilot1.jpa.dao.marque;

import java.util.Collection;

import ilot1.jpa.dao.AbstractDao;
import ilot1.jpa.entity.Marque;
import ilot1.jpa.entity.Modele;

public class MarqueDao extends AbstractDao<Marque> {
	
	public Collection<Marque> findAll() {
		return this.findAllByNamedQuery("findAllma");	
	}

}
