package ilot1.jpa.service.voiture;

import ilot1.jpa.dao.voiture.IVoitureDao;
import ilot1.jpa.dao.voiture.VoitureDao;
import ilot1.jpa.dto.VoitureCreationDto;
import ilot1.jpa.dto.retour.CodeRetour;
import ilot1.jpa.dto.retour.RetourDto;
import ilot1.jpa.entity.Voiture;
import ilot1.jpa.service.exception.AjoutInvalideException;

public class AjoutVoiture {
	private static IVoitureDao voiture;

	public static RetourDto ajouterUneVoiture(VoitureCreationDto vp) {
		try {
			voiture = new VoitureDao();
			MethodesVoiture.ajoutVoiture(vp.getModele(), vp.getCouleur());
			voiture.add(Voiture.builder().modele(vp.getModele()).couleur(vp.getCouleur()).build());
			return RetourDto.builder().code(CodeRetour.OK).meg("Ajout réussie").build();
		} catch (NullPointerException npe) {
			return RetourDto.builder().code(CodeRetour.NPE)
					.meg("Ajout impossible, la table modèle est vide et/ou la table couleur est vide.").build();
		} catch (AjoutInvalideException aie) {
			return RetourDto.builder().code(CodeRetour.AIE)
					.meg("Ajout impossible, ce modèle n'existe pas et/ou cette couleur n'existe pas.").build();
		}
	}
}
