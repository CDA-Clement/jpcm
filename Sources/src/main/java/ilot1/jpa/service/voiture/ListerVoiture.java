package ilot1.jpa.service.voiture;

import java.util.Collection;

import ilot1.jpa.dao.voiture.VoitureDao;
import ilot1.jpa.entity.Voiture;

public class ListerVoiture {
	private static VoitureDao voiture;
	private static Collection<Voiture> voitures;

	public static Collection<Voiture> listerVoiture() {
		voiture = new VoitureDao();
		voitures = voiture.findAll();
		voiture.close();
		return voitures;				
	}
}
