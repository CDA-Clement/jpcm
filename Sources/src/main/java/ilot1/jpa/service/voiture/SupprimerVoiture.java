package ilot1.jpa.service.voiture;

import ilot1.jpa.dao.voiture.VoitureDao;

public class SupprimerVoiture {
	private static VoitureDao voiture;
	
	public static void supprimerVoiture(Integer matricule) {
		voiture = new VoitureDao();
		voiture.remove(matricule);
		voiture.close();
	}
}
