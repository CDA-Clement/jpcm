package ilot1.jpa.service.voiture;

import java.util.Collection;

import ilot1.jpa.dao.couleur.CouleurDao;
import ilot1.jpa.dao.modele.ModeleDao;
import ilot1.jpa.dao.voiture.VoitureDao;
import ilot1.jpa.entity.Couleur;
import ilot1.jpa.entity.Modele;
import ilot1.jpa.service.exception.AjoutInvalideException;
import ilot1.jpa.service.exception.ListeVideException;

public abstract class MethodesVoiture extends Exception {
	private static final long serialVersionUID = 1L;
	private static CouleurDao couleurDao;
	private static ModeleDao modeleDao;
	private static VoitureDao voitureDao;
	private static Collection<Couleur> couleurs;
	private static Collection<Modele> modeles;

	public static void ajoutVoiture(String model, String color) throws AjoutInvalideException {
		couleurDao = new CouleurDao();
		modeleDao = new ModeleDao();
		boolean couleurExiste = false;
		boolean modeleExiste = false;
		couleurs = couleurDao.findAll();
		for (Couleur couleur : couleurs) {
			if (couleur.getLabel().equals(color)) {
				couleurExiste = true;
			}
		}

		if (!couleurExiste) {
			modeleDao.close();
			couleurDao.close();
			throw new AjoutInvalideException();
		}

		modeles = modeleDao.findAll();
		for (Modele modele : modeles) {
			if (modele.getLabel().equals(model)) {
				System.out.println("4");
				modeleExiste = true;
			}

		}

		if (!modeleExiste) {
			modeleDao.close();
			couleurDao.close();
			throw new AjoutInvalideException();
		}
		modeleDao.close();
		couleurDao.close();
	}

	public static void listerVoiture() throws ListeVideException {
		voitureDao = new VoitureDao();
		if(voitureDao.findAll().size() == 0) {
			throw new ListeVideException();
		}
		voitureDao.close();
	}
}
