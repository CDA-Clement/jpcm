package ilot1.jpa.service.marque;

import ilot1.jpa.dao.marque.MarqueDao;
import ilot1.jpa.entity.Marque;

public class AjoutMarque {
	private static MarqueDao marque;

	public static String ajouterUneMarque(String label) {
		try {
			marque = new MarqueDao();
			marque.add(Marque.builder().label(label).build());
			marque.close();
			return "Ajout réussie";
		} catch (NullPointerException npe) {
			marque.close();
			return "Erreur code NPE : Ajout impossible, la table modèle est vide et/ou la table couleur est vide.";
		}
	}
}
