package ilot1.jpa.service.marque;

import java.util.Collection;

import ilot1.jpa.dao.marque.MarqueDao;
import ilot1.jpa.entity.Marque;

public class ListerMarque {
	private static MarqueDao marque;
	private static Collection<Marque> marques;

	public static Collection<Marque> listerMarque() {
		marque = new MarqueDao();
		marques = marque.findAll();
		marque.close();
		return marques;
	}
}
