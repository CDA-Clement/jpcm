package ilot1.jpa.service.couleur;

import ilot1.jpa.dao.couleur.CouleurDao;
import ilot1.jpa.entity.Couleur;

public class AjoutCouleur {
	private static CouleurDao couleur;

	public static String ajouterUneCouleur(String label) {
		try {
			couleur = new CouleurDao();
			couleur.add(Couleur.builder().label(label).build());
			couleur.close();
			return "Ajout réussie";
		} catch (NullPointerException npe) {
			couleur.close();
			return "Erreur code NPE : Ajout impossible, la table modèle est vide et/ou la table couleur est vide.";
		}
	}
}
