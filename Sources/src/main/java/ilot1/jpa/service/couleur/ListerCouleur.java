package ilot1.jpa.service.couleur;

import java.util.Collection;

import ilot1.jpa.dao.couleur.CouleurDao;
import ilot1.jpa.entity.Couleur;

public class ListerCouleur {
	private static CouleurDao couleur;
	private static Collection<Couleur> couleurs;

	public static Collection<Couleur> listerCouleur() {
		couleur = new CouleurDao();
		couleurs = couleur.findAll("findAllc");
		couleur.close();
		return couleurs;
	}
}
