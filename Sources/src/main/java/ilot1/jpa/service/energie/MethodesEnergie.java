package ilot1.jpa.service.energie;

import ilot1.jpa.dao.marque.MarqueDao;
import ilot1.jpa.service.exception.ListeVideException;

public abstract class MethodesEnergie extends Exception {
	private static final long serialVersionUID = 1L;
	private static MarqueDao marqueDao;
	
	public static void listerMarque() throws ListeVideException {
		marqueDao = new MarqueDao();
		if(marqueDao.findAll().size() == 0) {
			throw new ListeVideException();
		}
		marqueDao.close();
	}
}
