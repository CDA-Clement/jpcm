package ilot1.jpa.service.energie;

import ilot1.jpa.dao.energie.EnergieDao;
import ilot1.jpa.entity.Energie;

public class AjoutEnergie {
	private static EnergieDao energie;

	public static String ajouterUneEnergie(String label) {
		try {
			energie = new EnergieDao();
			energie.add(Energie.builder().label(label).build());
			energie.close();
			return "Ajout réussie";
		} catch (NullPointerException npe) {
			energie.close();
			return "Erreur code NPE : Ajout impossible, la table modèle est vide et/ou la table couleur est vide.";
		}
	}
}
