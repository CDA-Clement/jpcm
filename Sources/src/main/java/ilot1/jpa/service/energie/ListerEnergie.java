package ilot1.jpa.service.energie;

import java.util.Collection;

import ilot1.jpa.dao.energie.EnergieDao;
import ilot1.jpa.entity.Energie;

public class ListerEnergie {
	private static EnergieDao energie;
	private static Collection<Energie> energies;

	public static Collection<Energie> listerEnergie() {
		energie = new EnergieDao();
		energies = energie.findAll();
		energie.close();
		return energies;
	}
}
