package ilot1.jpa.service.modele;

import ilot1.jpa.dao.modele.ModeleDao;
import ilot1.jpa.entity.Modele;
import ilot1.jpa.service.exception.AjoutInvalideException;

public class AjoutModele {
	private static ModeleDao modele;

	public static String ajouterUnModele(String marque, String label, Integer puissance, String energie) {
		try {
			modele = new ModeleDao();
			MethodesModele.ajoutModele(marque);
			modele.add(Modele.builder().marque(marque).label(label).puissance(puissance).energie(energie).build());
			modele.close();
			return "Ajout réussie";
		} catch (NullPointerException npe) {
			modele.close();
			return "Erreur code NPE : Ajout impossible, la table modèle est vide et/ou la table couleur est vide.";
		} catch (AjoutInvalideException aie) {
			modele.close();
			return "Erreur code AIE : Ajout impossible, la marque n'existe pas.";
		}
	}
}
