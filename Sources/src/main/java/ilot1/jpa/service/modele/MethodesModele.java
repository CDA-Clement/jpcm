package ilot1.jpa.service.modele;

import java.util.Collection;

import ilot1.jpa.dao.energie.EnergieDao;
import ilot1.jpa.dao.marque.MarqueDao;
import ilot1.jpa.entity.Marque;
import ilot1.jpa.service.exception.AjoutInvalideException;
import ilot1.jpa.service.exception.ListeVideException;

public abstract class MethodesModele extends Exception {
	private static final long serialVersionUID = 1L;
	private static EnergieDao energieDao;
	private static MarqueDao marqueDao;
	private static Collection<Marque> marques;

	public static void listerEnergie() throws ListeVideException {
		energieDao = new EnergieDao();
		if(energieDao.findAll().size() == 0) {
			throw new ListeVideException();
		}
		energieDao.close();
	}

	protected static void ajoutModele(String brand) throws AjoutInvalideException {
		marqueDao = new MarqueDao();
		boolean marqueExiste = false;
		marques = marqueDao.findAll();
		for (Marque marque : marques) {
			if (marque.getLabel().equals(brand)) {
				marqueExiste = true;
			}
		}

		if (!marqueExiste) {
			marqueDao.close();
			throw new AjoutInvalideException();
		}
	}
}
