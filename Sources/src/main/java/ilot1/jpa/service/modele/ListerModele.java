package ilot1.jpa.service.modele;

import java.util.Collection;

import ilot1.jpa.dao.modele.ModeleDao;
import ilot1.jpa.entity.Modele;

public class ListerModele {
	private static ModeleDao modele;
	private static Collection<Modele> modeles;

	public static Collection<Modele> listerModele() {
		modele = new ModeleDao();
		modeles = modele.findAll("findAllm");
		modele.close();
		return modeles;
	}
}
