package ilot1.jpa.service;

import java.util.Collection;

public interface IService<T> {
	public void remove(Object pk);
	public T update(T entity);
	public T add(T entity);
	public Collection<T> findAll();
}
